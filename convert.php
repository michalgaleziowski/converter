<?php
/* Karuzela bez użycia JavaScript, wyświetlająca pobrane 'Kupili rowniez' 	*/
/* Autor: Michał Gałęziowski 												*/

/* Domyślny numer aukcji, jesli nei można pobrać z linku */
	$matches = array('181869643012');

/* Pobieramy numer aukcji z linku */
	if(isset($_SERVER['REQUEST_URI'])){
		$auction_id = $_SERVER['REQUEST_URI'];
		preg_match('/\\d{12}/', $auction_id, $matches);
	}
	
	$host = '/itm/';
/* Pobranie propozycji dla konkretnej aukcji */
	require_once('databaseConnection.class.php');
	$conn = new databaseConnection;
	$results = $conn->runQuery('call app_othersbought.show_proposals('.$matches[0].')');
	

/* Przygotowanie rozpiski ktore propozycje maja byc wyswietlone w karuzeli 													*/
/* carousel - jest to numer 'wagonika', zawiera tablice z numerami aukcji, ustawionymi tak, aby przesuwanie pozwalalo		*/
/* zachować zasade działania karuzeli 																						*/
	$all = array(
					'carousel-1' => array(5,0,1),
					'carousel-2' => array(0,1,2),
					'carousel-3' => array(1,2,3),
					'carousel-4' => array(2,3,4),
					'carousel-5' => array(3,4,5),
					'carousel-6' => array(4,5,0)
				);

?>


<?php
require_once('html2_pdf_lib/html2pdf.class.php');


	$a = 'checked="checked"';
	$html_content = '

	<html>
	<head>
		<link rel="Stylesheet" type="text/css" href="style.css" />
	</head>
	<body>
		<div id=\'container\'>
			<div class="carousel">
	   			<div class="carousel-inner"> 

					<input class="carousel-open" type="radio" id="'. 1 . '" name="carousel" aria-hidden="true" hidden="" '.$a.'>
					 	<div class="carousel-item">
						    <div class=\'item_to_show_left\' id=\'catB\' >
						       	<a class="not-active" href="'. $host . $results[0]['auction_id'] .'">
						          	<div class=\'polaroid\'>
						           		<img src='.$results[0]['pictureurl'].' />
					    	       		<div class=\'auction_title\'>
					        	   			'.$results[0]['title'] . '<br />
					            	   	</div>
					            		<div class=\'price\'>
					            			' . $results[0]['price'].' ' . $results[0]['currency'] .'
						            	</div> 
						           	</div>	
						       	</a>
						      	<div class=\'mirror\'></div>
						    </div>
						</div>
	    		</div>
			</div>
		</div>
	</body>
	</html>';



	file_put_contents('temp.html', $html_content);


?>

